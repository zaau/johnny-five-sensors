var five = require("johnny-five");
var board = new five.Board({port:"COM6"});

board.on("ready", function() {
  var ledBrightness = 0;

  var led1 = new five.Led(9);
  var led2 = new five.Led(12);
  var led3 = new five.Led(11);
  var led4 = new five.Led(10);

  var button2Pressed = false;

  button = new five.Button(4);
  button1 = new five.Button(6);
  button2 = new five.Button(5);
  button3 = new five.Button(7);

  board.repl.inject({
    button: button,
    button1:button1,
    button2:button2,
    button3:button3
  });

  button.on("down", function() {
    led1.fade(ledBrightness, 500);
  });
  button.on("up", function() {
      led1.fade(255,500);
  });

  button1.on("up", function() {
    if(button2Pressed){
      led2.stop();
      button2Pressed = false;
    }else{
      led2.blink(100);
      button2Pressed = true;
    }
  });

  button2.on("down", function() {
    led3.stop();
  });
  button2.on("up", function() {
    led3.blink(100);
  });
  button3.on("down", function() {
    led4.stop();
  });
  button3.on("up", function() {
    led4.blink(100);
  });

potentiometer = new five.Sensor({
    pin: "A2",
    freq: 250
  });

  // Inject the `sensor` hardware into
  // the Repl instance's context;
  // allows direct command line access
  board.repl.inject({
    pot: potentiometer
  });

  // "data" get the current reading from the potentiometer
  potentiometer.on("change", function() {
    
    ledBrightness = Math.round(((this.value * 100 /1023) * 255)/100);
    console.log(this.value, this.raw,ledBrightness);
    led1.brightness(ledBrightness);
  });

});
