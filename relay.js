var five = require("johnny-five"),
    board, lcd;

board = new five.Board({ port: "COM6" });

board.on("ready", function () {
    var relay = new five.Relay({ pin: 10, type: "NO" });
    this.repl.inject({
        relay: relay
    });
    relay.off();

    var humidity = new five.Sensor({pin:7, type:"digital",});

    humidity.on("change", function(){
        console.log("value: ", humidity.value + " raw: " + this.raw)
    })

});