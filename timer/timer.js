let setup = function(relay, dayChek){

    var data = {
        dateLong: 24,
        currentDate:0,
        newDate:3,
        currentDayInterval: [18, 6],
        newDayInterval:[12,12],
        intervals:[
            {
                date:"2017-07-28",
                dayInterval:[18, 6]
            },
            {
                date:"2017-08-11",
                dayInterval:[24, 0]
            }
        ],
        stepper:0.25
    }
    function init(changeDuration, daySetup){
        /*
            dayCycle:[18,6],
            
        */
        data.newDate = data.currentDate + changeDuration;
        data.newDayInterval = daySetup;
    }

    function updateTimer(timeStep){
        data.currentDate += 1;
        data.currentDayInterval = [17.35, 6.35];
    }

    function start(dayCycle = [18,6], timeStep = 25, currentDate = 0){

        if(data.newDate == currentDate){
            return;
        } 

        let newInterval = dayCycle.map( function(time, i){
            return i % 2 ? time - timeStep : time + timeStep;   
        });
        console.log(`NewInterval ${newInterval}`);

        var timeOutTime = 0;

        if(dayChek.isDay()){
            dayCheck.off();
            relay.on();
            timeOutTime= newInterval[0];
            data.currentDate += 1;
        } else {
            dayCheck.off();
            relay.off();
            timeOutTime = newInterval[1];
        }


        setInterval(function(){
             start( newInterval, timeStep, data.currentDate);
        },
        timeOutTime*60*60*1000);
    }

    return {
        start:start(),
        init:init()
    }
};



var dayCheck = function(){
        let status = false;
        return {
            isDay:function(){
                console.log("status: ", status);
                return status;
            },
            on:function(){
                status = true;
                console.log("status: ", status);
            },
            off:function(){
                status = false;
                console.log("status: ", status);
            }
        }
    }

var relay = function(){
    let status = false;
    return {
        isOn:function(){
            console.log("status: ", status);
            return status;
        },
        on:function(){
            status = true;
            console.log("status: ", status);
        },
        off:function(){
            status = false;
            console.log("status: ", status);
        }
    }
    
}

var timer = setup(relay(), dayCheck());
timer.start();
